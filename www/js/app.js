// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('cidadeMaisSegura', ['ionic']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});


app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('login', {
    url: '/',
    templateUrl: 'views/login.html',
    controller: 'Login',
    cache: false
  });

  $stateProvider.state('Cadastre-se', {
    url: '/cadastre-se',
    templateUrl: 'views/cadastre-se.html',
    controller: 'Cadastre-se',
    cache: false
  });

  $stateProvider.state('findCop', {
    url: '/find-cop',
    templateUrl: 'views/find-cop.html',
    controller: 'FindCop',
    cache: false
  });

  $stateProvider.state('findCopForm', {
    url: '/find-cop/form',
    templateUrl: 'views/find-cop-form.html',
    controller: 'FindCopForm',
    cache: false
  });

  $stateProvider.state('Sucesso', {
    url: '/find-cop/sucesso',
    templateUrl: 'views/find-cop-sucesso.html',
    controller: 'FindCopSucesso',
    cache: false
  });

  $stateProvider.state('Falha', {
    url: '/find-cop/falha',
    templateUrl: 'views/find-cop-falha.html',
    controller: 'FindCopFalha',
    cache: false
  });


});


app.controller("contentController", function($scope, $ionicSideMenuDelegate, $window) {

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.changeLocation = function(url) {
    if ( url != "#")
      $window.location =  url;

    $ionicSideMenuDelegate.toggleLeft();
  };


});


app.controller("Login", function($scope, $rootScope) {
  $rootScope.title = null;
});

app.controller("Cadastre-se", function($scope, $rootScope) {
  $rootScope.title = null;
});

app.controller("FindCop", function($scope, $rootScope) {
  $rootScope.title = "Solicitar polícia";
});

app.controller("FindCopForm", function($scope, $rootScope) {
  $rootScope.title = "Solicitar polícia";
});

app.controller("FindCopSucesso", function($scope, $rootScope) {
  $rootScope.title = "Solicitação enviada";
});

app.controller("FindCopFalha", function($scope, $rootScope) {
  $rootScope.title = "Falha na Solicitação";
});

